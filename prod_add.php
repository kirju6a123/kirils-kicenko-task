<?php 
require_once('model.php');
$model = new model();

?>

<!doctype html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link rel="stylesheet" href="css/styles.css" />
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
    <title>Add product</title>
</head>
<body>
    <div class="container">
        <div class="row">
            <div class="col-md-12 mt-5">
              <h1 class="text-center">Product Add</h1>
                <hr>
            </div>
        </div>

        <div class="row">
            <div class="col-md-5 mx-auto">
                <?php $model->storeRecord(); ?>
                <form action="" method="post">
                    <div class="form-group">
                        <label for="">SKU</label>
                        <input type="text" name="SKU" class="form-control">
                    </div>
                    <div class="form-group">
                        <label for="">Name</label>
                        <input type="text" name="Name" class="form-control">
                    </div>
                    <div class="form-group">
                        <label for="">Price</label>
                        <input type="text" name="Price" class="form-control">
                    </div>
                    <div class="form-group">
                        <select>
                            <option>Type switch</option>
                            <option value="size">Size</option>
                            <option value="dimension">Dimension</option>
                            <option value="weight">Weight</option>
                        </select>
                    </div>
                    <div class="size box">
                        <label>Size</label>
                        <input class="form-control" type="text" name="Size">
                    </div>
                    <div class="dimension box">
                        <label>Height</label><input class="form-control" type="text" name="Height" >
                        <br><label>Width</label><input class="form-control" type="text" name="Width" >
                        <br><label>Length<input class="form-control" type="text" name="Length" >
                    </div>
                    <div class="weight box">
                        <label>Weight</label><input class="form-control" type="text" name="Weight" >
                    </div>
                    <br>
                    <input type="submit" name="submit" class="btn btn-primary" value="Save">
                    <a href="index.php" class="btn btn-default">Cancel</a>
                </form>
            </div>
        </div>
    </div>
    <script src="https://code.jquery.com/jquery-1.12.4.min.js"></script>
    <script src="js/custom.js"></script>
</body>

</html>