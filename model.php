<?php

class Model
{
    /**
     * Attempt to connect to MySQL database
     */
    private $server = "localhost";
    private $username = "root";
    private $password = "root";
    private $db = "junior";
    private $conn;

    public function __construct()
    {
        try {
            $this->conn = new mysqli($this->server, $this->username, $this->password, $this->db);
        } catch (Exception $e) {
            echo "connection failed" . $e->getMessage();
        }
    }

    /**
     * check valid SQL strings
     */
    public function check($a)
    {
        $return = mysqli_real_escape_string($this->conn, $a);
        return $return;
    }

    /**
     * inserting dates in DB
     */
    public function storeRecord()
    {
        if (isset($_POST['submit'])) {
            $SKU = $this->check($_POST['SKU']);
            $Name = $this->check($_POST['Name']);
            $Price = $this->check($_POST['Price']);
            $Size = $this->check($_POST['Size']);
            $Height = $this->check($_POST['Height']);
            $Width = $this->check($_POST['Width']);
            $Length = $this->check($_POST['Length']);
            $Weight = $this->check($_POST['Weight']);

            if ($this->insertRecord($SKU, $Name, $Price, $Size, $Height, $Width, $Length, $Weight)) {
                echo '<div class="alert alert-success"> Your Record Has Been Saved :) </div>';

            } else {

                echo '<div class="alert alert-danger"> Failed </div>';
            }
        }
    }

    /**
     * Check input errors before inserting in database *
     */
    public function insertRecord($SKU, $Name, $Price, $Size = null, $Height = null, $Width = null, $Length = null, $Weight = null)
    {
        $query = "insert into products (SKU, Name, Price, Size, Height, Width, Length, Weight) values('$SKU', '$Name', '$Price', '$Size', '$Height', '$Width', '$Length', '$Weight')";
        $result = mysqli_query($this->conn, $query);

        return $result;
    }

    /**
     * Deleting dates from DB
     */
    public function massDelete()
    {
      foreach($_POST['id'] as $id){
        $query = "DELETE FROM products WHERE id = " . $id;
      
        mysqli_query($this->conn, $query);
      }
        header("location:index.php");
    }

    /**
     * Attempt select query execution
     */
    public function fetch()
    {
        $data = null;
        $query = "SELECT * FROM products";

        if ($sql = $this->conn->query($query)) {
            while ($row = mysqli_fetch_assoc($sql)) {
                $data[] = $row;
            }
        }
        return $data;
    }


}


