<?php

require_once("model.php");
$model = new Model();
$rows = $model->fetch();
 ?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Dashboard</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.css">
    <link rel="stylesheet" href="scss/style.css" />
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.js"></script>
    <script type="text/javascript">
        $(document).ready(function(){
            $('[data-toggle="tooltip"]').tooltip();   
        });
    </script>
</head>
<body>
    <div class="wrapper">
        <div class="container-fluid">
            <div class="col-md-12">
                <div class="page-header clearfix">
                    <h2 class="pull-left">Product List</h2>
                    <a href="prod_add.php" class="btn btn-success pull-right">Add New Product</a>
                    <button type="submit" form="productList" class="btn btn-danger pull-right">Mass Delete Action</button>
                </div>
            </div>
            <div class="row">
                    <div class="form-check">
                        <form id="productList" action="" method="POST">
                            <div class="rowV2-39-final">
                                <?php
                                    if (isset($_POST['id'])) {
                                        $model->massDelete();
                                    }

                                $i = 1;
                                if (!empty($rows)){ ?>
                                
                                    <?php foreach ($rows as $key => $row){ ?>
                                    <div class = "panel panel-default">
                                        <div class="panel-body">
                                            <div class="form-check">
                                                <input type="checkbox" name="id[]" value="<?= $row['id']?>">
                                                <div><?php echo $row['SKU']; ?> </div>
                                                <div><?php echo $row['Name']; ?> </div>
                                                <div>$<?php echo $row['Price']; ?> </div>
                                                <div><?php echo $row['Size']; ?> </div>
                                                <div><?php echo $row['Height'] . $row['Width'] .$row['Length']; ?> </div>
                                                <div><?php echo $row['Weight']; ?> </div>
                                            </div>
                                        </div>
                                    </div>
                                <?php } ?>
                            </div>
                        </form>
                        <?php } ?>
                </div>
            </div>
        </div>
    </div>
</body>
</html>